<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/users', 'UserController@index')->name('users');
Route::post('/new-user', 'UserController@store')->name('new-user');
Route::delete('/delete-user/{id}', 'UserController@destroy')->name('delete-user');
Route::put('/edit-user/{id}', 'UserController@update')->name('edit-user');
Route::get('/edit-user/{id}', 'UserController@show')->name('edit-user');
