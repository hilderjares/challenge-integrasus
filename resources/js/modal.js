$("#userModal").on("show.bs.modal", function (event) {

    const button = $(event.relatedTarget);
    const action = button.attr("data-action");
    const type = button.attr("data-type");

    const modal = $(this);
    modal.find(".modal-body form").attr("action", action);

    if (type === "edit") {
        modal.find(".modal-body form #method").val("PUT");

        $.ajax({
            type: "GET",
            url: action,
            success: (data) => {
                modal.find(".modal-body form #fullName").val(data.client.full_name ?? "");
                modal.find(".modal-body form #cpf").val(data.client.cpf ?? "");
                modal.find(".modal-body form #whatsapp").val(data.client.whatsapp ?? "");
                modal.find(".modal-body form #age").val(data.client.age ?? "");
            },
            error: (error) => {
                console.log(error);
            },
        });
    }
});
