<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <title>List Clients</title>
</head>
<body>
<div class="container-fluid main">
    <h3 class="text-center"> SUS CE 2020 </h3>

    @include('user.modal')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div
    @endif

    <div class="card">
        <div class="card-header">
            Usuários
            <button type="button" class="btn btn-primary" data-toggle="modal" data-action="{{ route('new-user') }}" data-target="#userModal">
                Novo
            </button>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome Completo</th>
                    <th scope="col">Whatsapp</th>
                    <th scope="col">Idade</th>
                    <th scope="col">CPF</th>
                    <th scope="col">Imagem</th>
                    <th scope="col">Ações</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->full_name }}</td>
                            <td>{{ $user->whatsapp }}</td>
                            <td>{{ $user->age }}</td>
                            <td>{{ $user->cpf }}</td>
                            <td><img src="{{ asset($user->photo) }}" width="50" height="50" /></td>
                            <td>
                                <form action="{{ route('delete-user', $user->id) }}" method="post" onsubmit="confirm('tem certeza disso ?')">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">DELETE</button>
                                </form>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-type="edit" data-action="{{ route('edit-user', $user->id) }}" data-target="#userModal">
                                    Editar
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
</body>
</html>