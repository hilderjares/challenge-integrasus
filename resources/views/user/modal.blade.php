<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="userModalLabel">Novo Usuário</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="userForm" action="#" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="fullName">Nome Completo</label>
                        <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Nome Completo">
                    </div>
                    <div class="form-group">
                        <label for="cpf">CPF</label>
                        <input type="text" class="form-control" id="cpf" name="cpf" placeholder="CPF">
                    </div>
                    <div class="form-group">
                        <label for="age">Idade</label>
                        <input type="number" class="form-control" id="age" name="age" placeholder="Idade">
                    </div>
                    <div class="form-group">
                        <label for="whatsapp">WhatsApp</label>
                        <input type="tel" class="form-control" id="whatsapp" name="whatsapp" placeholder="WhatsApp">
                    </div>
                    <div class="form-group">
                        <label for="photo">Foto</label>
                        <input type="file" class="form-control" id="photo" name="photo" placeholder="Foto">
                    </div>
                    <input type="hidden" name="_token" value="{{ @csrf_token() }}">
                    <input type="hidden" name="_method" value="POST" id="method">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
