<?php

namespace App\Repositories\EloquentRepository;

use App\Exceptions\UserNotFoundException;
use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Support\Collection;

final class UserRepository implements UserRepositoryInterface
{
    public function create(array $data): User
    {
        $user = new User();
        $user->full_name = $data['fullName'] ?? "";
        $user->cpf = $data['cpf'] ?? "";
        $user->age = $data['age'] ?? "";
        $user->whatsapp = $data['whatsapp'] ?? "";

        if ($data['photo']) {
            $user->photo = $data['photo'];
        }

        $user->save();

        return $user;
    }

    public function update(User $user, array $data): User
    {
        $user->full_name = $data['fullName'] ?? "";
        $user->cpf = $data['cpf'] ?? "";
        $user->age = $data['age'] ?? "";
        $user->whatsapp = $data['whatsapp'] ?? "";
        $user->photo = $data['photo'] ?? "";
        $user->save();

        return $user;
    }


    public function getAll(): Collection
    {
        $users = User::all();

        return $users;
    }

    public function search(int $id): User
    {
        $user = User::find($id);

        if (!$user) {
            throw new UserNotFoundException('não foi possível encontrar o usuário');
        }

        return $user;
    }

    public function delete(int $id): User
    {
        $user = $this->search($id);
        $user->delete();

        return $user;
    }
}