<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Repositories\EloquentRepository\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

final class UserController extends Controller
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        return response()->view('user.list', ['users' => $this->userRepository->getAll()]);
    }

    public function store(UserRequest $request)
    {
        $data = $request->request->all();
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo')->store('avatars');
            $data['photo'] = $photo;
        }

        $user = $this->userRepository->create($data);

        return redirect()->route('users');
    }

    public function show(int $id)
    {
        return response()->json(['client' => $this->userRepository->search($id)]);
    }

    public function update(Request $request, int $id)
    {
        $user = $this->userRepository->search($id);

        $data = $request->request->all();
        if ($request->hasFile('photo') && $request->file('photo')->isFile()) {
            $photo = $request->file('photo')->store('avatars');
            $data['photo'] = $photo;
        }

        $newUser = $this->userRepository->update($user, $data);

        return redirect()->route('users');
    }

    public function destroy(int $id)
    {
        $user = $this->userRepository->search($id);
        $photo = $user->photo ?? "";
        Storage::delete($photo);

        $this->userRepository->delete($id);

        return redirect()->route('users');
    }
}
