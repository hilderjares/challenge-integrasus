<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fullName' => 'required|min:10',
            'cpf' => 'cpf|unique:users|required',
            'whatsapp' => 'unique:users|required',
        ];
    }
}
